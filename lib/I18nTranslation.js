const { resolve, extname, dirname, basename } = require('path')
const { readdir, stat, readFile, writeFile } = require('fs').promises
const { existsSync } = require('fs')
const mkdir = require('mkdirp')
const po2json = require('po2json')

class I18nTranslation {
  constructor (options = {}) {
    console.log('I18nTranslation init!')
    const defaultOptions = {
      from: './locale/',
      to: null,
      pretty: false
    }
    this.options = Object.assign(defaultOptions, options)
  }

  run () {
    this.locales().then((list) =>{
      list.forEach((file) => {
        return this.translateFile(file)
      })
    })
  }

  translateFile (file) {
    let json = {}
    readFile(file).then((buffer) => {
      let jsonData = po2json.parse(buffer)
      for (let key in jsonData) {
        // Special headers handling, we do not need everything
        if ('' === key) {
          json[''] = {
            'language': jsonData['']['language'],
            'plural-forms': jsonData['']['plural-forms']
          }
          continue;
        }

        json[key] = 2 === jsonData[key].length ? jsonData[key][1] : jsonData[key].slice(1);
      }

      let saveTo = file.replace('.po', '.json')

      if (this.options.to) {
        let filename = basename(dirname(file))
        if (!existsSync(this.options.to)) {
          mkdir.sync(this.options.to)
        }
        saveTo = `${this.options.to}${filename}.json`
      }
      writeFile(saveTo, JSON.stringify(json, null, this.options.pretty ? 4 : 0)).then((err) => {
        if (err)
          console.log('I18nTranslation: writeFile error: ',err);
        else {
          console.log(' I18nTranslation: JSON ' + (this.options.pretty ? 'pretty' : 'compactly') + ' saved to ' + saveTo);
        }
      })
    })
  }

  async locales () {
    const files = await this.getFiles(this.options.from)
    return files.filter((f) => extname(f) == '.po' )
  }

  async getFiles(dir) {
    const subdirs = await readdir(dir);
    const files = await Promise.all(subdirs.map(async (subdir) => {
      const res = resolve(dir, subdir);
      return (await stat(res)).isDirectory() ? this.getFiles(res) : res;
    }));
    return Array.prototype.concat(...files);
  }
}

module.exports = I18nTranslation;
