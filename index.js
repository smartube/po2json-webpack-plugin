const path = require('path')
const I18nTranslation = require('./lib/I18nTranslation')

// const pluginName = 'I18nTranslationPlugin'

class I18nTranslationPlugin {

  constructor (options = {}) {
    const defaultOptions = {
      from: path.join(__dirname, './locale'),
      to: null
    }

    this.options = Object.assign(defaultOptions, options);
  }

  apply(compiler) {
    const translator = new I18nTranslation(this.options)

    return translator.run()
  }
}

I18nTranslationPlugin.VERSION = 1
module.exports = I18nTranslationPlugin;
